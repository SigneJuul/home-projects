﻿using System;

namespace StenSaksPapir
{
    class Program
    {
        static void Main(string[] args)
        {
            int exit;
            do
            {
                Console.Clear();
                Console.WriteLine("Hvad vælger du: \n1. Sten \n2. Saks \n3. Papir");
                int Userchoice = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                string[] choices = { "Sten", "Saks", "Papir" };

                Random rand = new Random();

                int index = rand.Next(choices.Length);

                switch (Userchoice)
                {
                    case 1:
                        if (choices[index] == choices[0]) //vælge ved at sammenligne med array-nummeret
                        {
                            Console.WriteLine($"Sten vs {choices[index]} \nUafgjort");
                        }
                        else if (choices[index] == "Saks")
                        {
                            Console.WriteLine($"Sten vs { choices[index]} \nTillyke, du vandt!");
                        }
                        else
                        {
                            Console.WriteLine($"Sten vs {choices[index]} \nDu tabte, beklager");
                        }
                        break;
                    case 2:
                        if (choices[index] == "Saks") //vælge ved at sammenligne med array-indholdet
                        {
                            Console.WriteLine($"Saks vs {choices[index]} \nUafgjort");
                        }
                        else if (choices[index] == "Papir")
                        {
                            Console.WriteLine($"Saks vs { choices[index]} \nTillyke, du vandt!");
                        }
                        else
                        {
                            Console.WriteLine($"Saks vs {choices[index]} \nDu tabte, beklager");
                        }
                        break;
                    case 3:
                        if (choices[index] == "Papir")
                        {
                            Console.WriteLine($"Papir vs {choices[index]} \nUafgjort");
                        }
                        else if (choices[index] == "Sten")
                        {
                            Console.WriteLine($"Papir vs { choices[index]} \nTillyke, du vandt!");
                        }
                        else
                        {
                            Console.WriteLine($"Papir vs {choices[index]} \nDu tabte, beklager");
                        }
                        break;
                }
                Console.WriteLine("\nPrøv igen? \n1. Ja \n2. Nej");
                exit = Convert.ToInt32(Console.ReadLine());
            } while (exit != 2);

            Console.ReadKey();
        }
    }
}
